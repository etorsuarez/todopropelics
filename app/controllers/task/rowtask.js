var args = arguments[0] || {};

var callbackCheckChange = args.callbackCheckChange;
var id          = args.id;
var content     = args.content;
var lastmoddate = args.lastmoddate;
var status      = args.status;
var image       = args.image;

// id of Task for table click event
$.Row.id = id;

fillData();

function fillData(){
	var lastDate 	= new Date(lastmoddate.substring(0,4) , lastmoddate.substring(4,6), lastmoddate.substring(6,8), lastmoddate.substring(8,10), lastmoddate.substring(10,12), lastmoddate.substring(12,14), 0);
	var minutes = (parseInt(lastDate.getMinutes()) < 10)? '0' + lastDate.getMinutes() : lastDate.getMinutes();

	$.contentLabel.text 	= content;
	$.lastmodLabel.text 	= L('LASTMOD') + ' ' + lastDate.toLocaleDateString() + ' ' + lastDate.getHours() + ':' + minutes;
	$.thumbnailImage.image 	= image;

	// Temporary code it will assign the image to show a checkbox  checked/unchecked
	if(status == 'Pending'){
		$.statusImage.image = '/images/checkbox_unchecked.png';
	} else {
		$.statusImage.image = '/images/checkbox_checked.png';
	}

}



// We create a checkZone so the user has more area to click on the check
$.checkZone.addEventListener('click', function(e){
	var todotask = Alloy.createCollection("todotask");
	todotask.fetch();
	
	var m = todotask.get(id);
	if(status == 'Pending'){
		m.set({
			status : 'Completed'
		}).save();
	} else {
		m.set({
			status : 'Pending'
		}).save();
	}

	callbackCheckChange();
});