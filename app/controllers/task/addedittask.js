// Controller for Adding and Editing tasks
// taskID .- If present it represents the id from todotask row to Edit
//			 If not present the task will be added to the database
// callback .- Callback to call when the task was added or edited

var args = arguments[0] || {};
var taskID = args.taskID || null;
var callback = args.callback;
var imagePath = "";
var modelTask;

if(taskID === null){
	$.Win.title = L("NEWTASK");
} else{
	$.Win.title = L("EDITTASK");
	$.photoButton.title = L('SELECTPHOTO');
	var todotask = Alloy.createCollection("todotask");
	todotask.fetch();

	var modelTask = todotask.get(taskID);
	$.taskText.value = modelTask.get("content");

	if(modelTask.get("image") != ''){
		$.photoImage.image = modelTask.get("image");
        imagePath = modelTask.get("image");
		$.photoImage.visible = true;
	}	
}

function saveTask(){
	if(taskID === null){
		addTask();	
	} else {
		editTask();
	}
}

// Function that add a Task to the model todotask
function addTask(){

	var currentDateTime = new Date(); 

	// Building the date string, also used to order lists by last modified date

	var month = currentDateTime.getMonth() + 1;
	month = (month < 10)? '0' + month : month;
	
	var day = currentDateTime.getDate();
	day = (day < 10)? '0' + day : day;

	var hour = currentDateTime.getHours();
	hour = (hour < 10)? '0' + hour : hour;

	var minute = currentDateTime.getMinutes();
	minute = (minute < 10)? '0' + minute : minute;

	var second = parseInt(currentDateTime.getSeconds());
	second = (second < 10)? '0' + second : second;

	var m = Alloy.createModel("todotask", {
		content 	: $.taskText.value,
		lastmoddate : currentDateTime.getFullYear() + month + day + hour + minute + second,
		status 		: 'Pending',
		image  		: imagePath
	});

	m.save();

	$.photoButton.image = null;

	callback();

	$.Win.close();
}

function editTask(){

	var currentDateTime = new Date(); 

	// Building the date string, also used to order lists by last modified date

	var month = currentDateTime.getMonth() + 1;
	month = (month < 10)? '0' + month : month;
	
	var day = currentDateTime.getDate();
	day = (day < 10)? '0' + day : day;

	var hour = currentDateTime.getHours();
	hour = (hour < 10)? '0' + hour : hour;

	var minute = currentDateTime.getMinutes();
	minute = (minute < 10)? '0' + minute : minute;

	var second = parseInt(currentDateTime.getSeconds());
	second = (second < 10)? '0' + second : second;

	modelTask.set({
		content 	: $.taskText.value,
		lastmoddate : currentDateTime.getFullYear() + month + day + hour + minute + second,
		image  		: imagePath
	}).save();

	$.photoButton.image = null;

	callback();

	Ti.App.fireEvent('taskUpdated');

	$.Win.close();

}



$.photoButton.addEventListener("click", function(){
	$.dialog.show();
});

$.dialog.addEventListener("click", function(e){
	var imageTool = require('imageapi');

	var imageDirectoryName = 'ToDoImages';
	var directory = Ti.Filesystem.getFile(Ti.Filesystem.getApplicationDataDirectory(), imageDirectoryName);

	if (!directory.exists()) {
		directory.createDirectory();
	};

	var paramsImage = {
		callback 				: callbackImageResult,
		destinationDirectory	: directory.resolve()
	};


	if(e.index == 0){  // Camera
		imageTool.getNewPhoto(paramsImage);
	} else if (e.index == 1) { // Gallery
		imageTool.getGalleryPhoto(paramsImage);
	}
});


function callbackImageResult(paramsImg){
	var result 			= paramsImg.result;
	var resultMessage 	= paramsImg.resultMessage; 

	if(result === true){
		Ti.API.info("");
		Ti.API.info(paramsImg.imagePath);

		$.photoImage.image = paramsImg.image;
		$.photoImage.visible = true;
		imagePath = paramsImg.imagePath;

	} else {
		if(resultMessage != 'cancel') {
			var dialog = Ti.UI.createAlertDialog({
			   	message: resultMessage,
			   	title: L('TODO')
		 	}).show();
		}
	}
}


