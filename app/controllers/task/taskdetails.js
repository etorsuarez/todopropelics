var args = arguments[0] || {};

var taskID 	     = args.id;
var callbackEdit = args.callbackEdit;
var modelTask;

fillData();


function fillData(){

	var todotask = Alloy.createCollection("todotask");
	todotask.fetch();

    modelTask = todotask.get(taskID);
    var lastmoddate = modelTask.get("lastmoddate");
    var lastDate    = new Date(lastmoddate.substring(0,4) , lastmoddate.substring(4,6), lastmoddate.substring(6,8), lastmoddate.substring(8,10), lastmoddate.substring(10,12), lastmoddate.substring(12,14), 0);
    var minutes = (parseInt(lastDate.getMinutes()) < 10)? '0' + lastDate.getMinutes() : lastDate.getMinutes();

    $.contentLabel.text = modelTask.get("content");
    $.lastmodLabel.text = lastDate.toLocaleDateString() + ' ' + lastDate.getHours() + ':' + minutes;
    $.statusLabel.text  = modelTask.get("status");

    if(modelTask.get("image") != ''){
        $.photoImage.image = modelTask.get("image");
        imagePath = modelTask.get("image");
        $.photoImage.visible = true;
    }
}

Ti.App.addEventListener('taskUpdated',fillData);

$.Win.addEventListener('close', function(){
    Ti.App.removeEventListener('taskUpdated', fillData);
});

if (OS_ANDROID ) {
    $.Win.addEventListener("open", function(e) {

        var activity = $.Win.getActivity();

        activity.onCreateOptionsMenu = function(e) {
            var itemEdit, itemShare, menu;
            menu = e.menu;
            menu.clear();

            itemEdit = menu.add({
                title : L('EDIT'),
                showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
            });

            itemEdit.addEventListener('click', function(e){
                editTask();
            });

            itemShare = menu.add({
                title : L('SHARE'),
                showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
            });

            itemShare.addEventListener('click', function(e){
            	$.dialogAndroid.show();
            });

        };
    });
 
    $.Win.addEventListener("focus", function(e) {
        $.Win.getActivity().invalidateOptionsMenu();
    });


    $.dialogAndroid.addEventListener("click", function(e){
        switch(e.index){
            case 0:         // Share SMS
                shareSMS();
                break;
            case 1:         // Share eMail
                shareeMail();
                break;
            case 2:         // Facebook
                shareFacebook();
                break;
        }
    });
}

if(OS_IOS){
	$.optionsButton.addEventListener('click', function(){
		$.dialogIOS.show();
	});

    $.dialogIOS.addEventListener("click", function(e){
            switch(e.index){
                case 0:         // Edit
                    editTask();
                    break;
                case 1:         // Share SMS
                    shareSMS();
                    break;
                case 2:         // Share eMail
                    shareeMail();
                    break;
                case 3:         // Facebook
                    shareFacebook();
                    break;
                case 4:         // Cancel
                    break;
            }
        });

    }

function editTask(){
    var paramsEdit = {
        taskID    : taskID,
        callback  : callbackEdit
    };
    callbackEdit(paramsEdit);
}

function shareSMS(){
    var shareAPI = require('shareapi');
    var params = {
        message : modelTask.get("content")
    };

    shareAPI.sendSMS(params);
}

function shareeMail(){
    var shareAPI = require('shareapi');
    var params = {
        subject : L('TODOSUBJECT'),
        message : modelTask.get("content")
    };

    if(modelTask.get("image") != ''){
        params.attachment = modelTask.get("image");
    }

    shareAPI.sendeMail(params);
}

function shareFacebook(){
    var shareAPI = require('shareapi');
    var params = {
        message : modelTask.get("content")
    };

    if(modelTask.get("image") != ''){
        params.attachment = modelTask.get("image");
    }

    shareAPI.sendFacebook(params);
}

$.photoImage.addEventListener('click', function(){
    var params = {
        image : imagePath
    };
    var taskImageViewer = Alloy.createController('imageViewer', params).getView();
    taskImageViewer.open();
});


