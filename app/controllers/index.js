


if (OS_ANDROID ) {

    // Android Action Bar Configuration
    $.tabGroup.addEventListener("open", function(e) {

        var activity = $.tabGroup.getActivity();

        activity.onCreateOptionsMenu = function(e) {
            var item, menu;
            menu = e.menu;
            menu.clear();

            if ($.tabGroup.activeTab == $.pendingTab) {
                item = menu.add({
                    title : L('ADD'),
                    showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
                });

                 item.addEventListener("click", function(){
                    addTask();
                });

            }
        };
    });
 
    $.tabGroup.addEventListener("focus", function(e) {
        $.tabGroup.getActivity().invalidateOptionsMenu();
    });
}


function addTask(){
    var params = {
        callback : callbackTaskAdd
    };

    var newTaskWindow = Alloy.createController('task/addedittask', params);
    
    $.pendingTab.open(newTaskWindow.getView());
}

// Callback to fill the Pending List
function callbackTaskAdd(){
    fillPending();
}




function fillPending(){
    var todotask = Alloy.createCollection("todotask");
    todotask.fetch({query:'SELECT * FROM todotask WHERE status="Pending" ORDER BY lastmoddate DESC'});

    var data = [];
    $.pendingTable.data = data;

    todotask.each(function(model){

        var params = {
            callbackCheckChange :  fillLists,
            id          : model.get("id"),
            content     : model.get("content"),
            lastmoddate : model.get("lastmoddate"),
            status      : model.get("status"),
            image       : model.get("image")
        };

        var rowTask = Alloy.createController("task/rowtask", params).getView();
        rowTask.filter = params.content;
        data.push(rowTask);
    });

    $.completedTable.filterAttribute = "filter";
    $.pendingTable.appendRow(data);
}

function fillCompleted(){
    var todotask = Alloy.createCollection("todotask");
    todotask.fetch({query:'SELECT * FROM todotask WHERE status="Completed" ORDER BY lastmoddate DESC'});

    var data = [];
    $.completedTable.data = data;

    todotask.each(function(model){

        var params = {
            callbackCheckChange :  fillLists,
            id          : model.get("id"),
            content     : model.get("content"),
            lastmoddate : model.get("lastmoddate"),
            status      : model.get("status"),
            image       : model.get("image")
        };

        var rowTask = Alloy.createController("task/rowtask", params).getView();
        rowTask.filter = params.content;
        data.push(rowTask);
    });

    $.completedTable.filterAttribute = "filter";
    $.completedTable.appendRow(data);
}

function fillLists(){
    fillPending();
    fillCompleted();
}

$.pendingTable.addEventListener('click', function(e){
    var params = {
        callbackEdit : callbackEditPending
    };

    if(e.row.id){
        params.id = e.row.id;
    } else {
        params.id = e.rowData.id;
    }

    var taskDetails = Alloy.createController('task/taskdetails', params);
    $.pendingTab.open(taskDetails.getView());
});

$.completedTable.addEventListener('click', function(e){
    var params = {
        callbackEdit : callbackEditCompleted
    };

    if(e.row.id){
        params.id = e.row.id;
    } else {
        params.id = e.rowData.id;
    }


    var taskDetails = Alloy.createController('task/taskdetails', params);
    $.completedTab.open(taskDetails.getView());
});

function callbackEditPending(paramsEdit){
    var params = {
        taskID   : paramsEdit.taskID,
        callback : callbackTaskUpdated
    };

    var newTaskWindow = Alloy.createController('task/addedittask', params);
    
    $.pendingTab.open(newTaskWindow.getView());    
}

function callbackEditCompleted(paramsEdit){
    var params = {
        taskID   : paramsEdit.taskID,
        callback : callbackTaskUpdated
    };

    var newTaskWindow = Alloy.createController('task/addedittask', params);
    
    $.completedTab.open(newTaskWindow.getView());    
}

function callbackTaskUpdated(){
    fillLists();
}

fillLists();

$.tabGroup.open();