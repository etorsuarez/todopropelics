
/*
	Open the eMail dialog with information from the parameters
	@param   params
				subject 	: text  contains the subject of the eMail 
				recipients 	: text[]  contains all the recipients to be deliver
				message 	: text  contains the message of the eMail body
				attachment 	: text  File to be attached to the eMail
*/
exports.sendeMail = function(params){
	var subject 	= params.subject || "";
	var recipients 	= params.recipients || [];
	var message 	= params.message || "";
	var attachment  = params.attachment || null;


	var emailDialog = Ti.UI.createEmailDialog();
	emailDialog.subject 	 = subject;
	emailDialog.toRecipients = recipients;
	emailDialog.messageBody  = message;

	if(attachment != null){
		var f = Ti.Filesystem.getFile(attachment);
		emailDialog.addAttachment(f);	
	}
	
	emailDialog.open();
}

/*
	Open the SMS dialog with the text to be sent
	@param   params
				message 	: text  contains the message of the SMS body
*/
exports.sendSMS = function(params){
	var message 	= params.message || "";

	Titanium.Platform.openURL('sms:?body=' + message);
}

/*
	Post a facebook status in the timeline of a user
	@param   params
				message 	: text  contains the message of the post
				attachment  : text  image to be posted
*/
exports.sendFacebook = function(params){
	var feedMessage 	= params.message || "";
	var attachment 		= params.attachment || null;

	var fb = require('facebook');

	if (OS_ANDROID)
		fb.forceDialogAuth = true;
	else
		fb.forceDialogAuth = false;

	fb.appid = 345076542321162;
	fb.permissions = ['create_event'];
	fb.authorize();

	// If there's NO an image attachment
	if(attachment == null){

		fb.requestWithGraphPath('me/feed', {message: feedMessage}, 
		         "POST", function(e) {
		    if (e.success) {
		        alert(L('FBSUCCESS'));
		    } else {
		        if (e.error) {
		            alert(L('ERRORFB') + '\n' +  e.error);
		        } else {
		            alert(L('UNKNOWNERROR'));
		        }
		    }
		});

	} else {

		var f = Ti.Filesystem.getFile(attachment);
		var blob = f.read();
		var data = {
		    message: feedMessage,
		    picture: blob
		};
		fb.requestWithGraphPath('me/photos', data, 'POST', function(e){
		    if (e.success) {
		        alert(L('FBSUCCESS'));
		    } else {
		        if (e.error) {
		            alert(L('ERRORFB') + '\n' + e.error);
		        } else {
		            alert(L('UNKNOWNERROR'));
		        }
		    }
		});


	}

}



