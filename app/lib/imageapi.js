
/*
	Open the camera to take a picture and return the image nativePath, image media and result status
	@param   params
				callback 				: text  function to be called back when the camera gets the photo, user cancel or camera error
				destinationDirectory 	: text  Directory to save the photo 
*/
exports.getNewPhoto = function(params){
	var callback 				= params.callback;
	var destinationDirectory 	= params.destinationDirectory;
	var resultMessage 			= "";

	Titanium.Media.showCamera({
		
		success:function(e) {

				
			var fileName = 'IMG_' + new Date().getTime() + ".png";
			
			var file = Ti.Filesystem.getFile(destinationDirectory, fileName);
			file.write(e.media);


			var paramsCallback = {
				result 			: true,
				resultMessage 	: "OK",
				image 			: e.media.imageAsResized(612, 612),
				imagePath 		: file.getNativePath()
			};

			callback(paramsCallback);

		},
		cancel:function() {
			resultMessage = "cancel";

			var paramsCallback = {
				result 			: false,
				resultMessage 	: resultMessage
			};

			callback(paramsCallback);			
		},
		error:function(e) {
			
			// Validate there's a camera in the device
			if(e.code == Titanium.Media.NO_CAMERA){
				resultMessage = L('NOCAMERA');
			} else {
				resultMessage = L('UNEXPECTEDERR') + '\n' + e.code;
			}

			var paramsCallback = {
				result 			: false,
				resultMessage 	: resultMessage
			};

			callback(paramsCallback);

		},
		saveToPhotoGallery:true,
		allowEditing:true,
		mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
	});
}


/*
	Open the photo gallery makes a copy of the file to the destination Directory and return the image nativePath, image media and result status
	@param   params
				callback 				: text  function to be called back when the user selects a image, user cancel or gallery error
				destinationDirectory 	: text  Directory to save the photo 
*/
exports.getGalleryPhoto = function(params){
	var callback 				= params.callback;
	var destinationDirectory 	= params.destinationDirectory;
	var resultMessage 			= "";

	Titanium.Media.openPhotoGallery({
		
		success:function(e) {

			var fileName = 'IMG_' + new Date().getTime() + ".png";

			var file = Titanium.Filesystem.getFile(destinationDirectory, fileName);
			file.write(e.media);

			var paramsCallback = {
				result 			: true,
				resultMessage 	: "OK",
				image 			: e.media.imageAsResized(612, 612),
				imagePath 		: file.getNativePath()
			};

			callback(paramsCallback);


		},
		cancel:function() {
			resultMessage = "cancel";

			var paramsCallback = {
				result 			: false,
				resultMessage 	: resultMessage
			};

			callback(paramsCallback);			
		},
		error:function(e) {

			var paramsCallback = {
				result 			: false,
				resultMessage 	: L('UNEXPECTEDERR') + '\n' + e.code
			};

			callback(paramsCallback);

		},
		saveToPhotoGallery:true,
		allowEditing:true,
		mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
	});
}
