exports.definition = {
	config: {
		columns: {
		    "id"			: "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "content"		: "TEXT",
		    "lastmoddate"	: "TEXT",
		    "status"  		: "TEXT",
		    "image"			: "TEXT"
		},
		adapter: {
			type: "sql",
			collection_name: "todotask",
			idAttribute : "id",
			db_name : "todopropelics"
		}
	},		
	extendModel: function(Model) {		
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});
		
		return Model;
	},
	extendCollection: function(Collection) {		
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});
		
		return Collection;
	}
};